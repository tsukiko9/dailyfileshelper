package main

import (
	"DailyFilesHelper/lib"
	"fmt"
)

const YamlFIlePath = "conf.yml"

func main() {
	lib.Logger("info", "FileHelper Version 1.0.3 Start...")
	var inputDate string
	println("【重要】测试版程序，请勿在生产环境使用！！！")
	print("请输入文件夹生成日期（格式YYYYMMDD）：")
	for {
		fmt.Scanln(&inputDate)
		if lib.DateCheck(inputDate) {
			lib.Logger("info", fmt.Sprintf("程序开始执行...目录创建日期为%v", inputDate))
			break
		} else {
			print("日期有误请重试（格式YYYYMMDD）：")
			continue
		}
	}
	lib.Core(YamlFIlePath, inputDate)
	println("按任意键关闭...")
	fmt.Scanln(&inputDate)
}
