package lib

import (
	"fmt"
	"os"
	"strings"
	"time"
)

func Logger(infoType string, msg string, args ...interface{}) {

	// 初始化时间变量
	currentTime := time.Now().Format("2006-01-02 15:04:05.000")
	currentDate := time.Now().Format("20060102")
	// 合并日志输出string
	mergeMsg := fmt.Sprintf("%v - [%v]: %v", currentTime, infoType, msg)

	// 若拼接错误信息
	if args != nil && infoType == "err" {
		mergeMsg = fmt.Sprintf("%v，错误信息：%v", mergeMsg, args)
	}

	if err := ensureDir("logs"); err != nil {
		fmt.Printf("创建logs文件夹报错，请检查，err: %v", err)
	}
	// 追加式写入日志文件
	logFile, err := os.OpenFile(fmt.Sprintf("logs\\%v.log", currentDate), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0664)
	// 无法写入日志文件错误输出到控制台
	if err != nil {
		fmt.Printf("打开log报错，请检查，err: %v", err)
	}
	// 写入日志
	mergeMsg += "\n"

	// 输出黑框信息
	if !strings.Contains(mergeMsg, "debug") {
		fmt.Print(mergeMsg)
	}

	if _, err = logFile.WriteString(mergeMsg); err != nil {
		fmt.Printf("写入log报错，请检查，err: %v", err)
	}
	// 关闭日志文件
	if err = logFile.Close(); err != nil {
		fmt.Printf("log文件关闭报错，请检查，err: %v", err)
	}
}
