package lib

import (
	"fmt"
	"reflect"
	"strings"
)

func Core(confPath string, date string) {
	// 读取配置文件
	conf, err := yamlReader(confPath)
	if err != nil {
		Logger("err", "无法读取配置文件，请检查conf.yml文件是否与可执行文件同级", err.Error())
		return
	} else {
		Logger("info", "YAML文件读取结束...")

	}

	// 配置文件输出
	if conf.Debug {
		Logger("info", "检测到调试开关打开，调试信息将输出到log文件中...")
		debugOutput(conf)
	}

	// 根据配置的文件组进行处理
	for _, item := range conf.FilesList {

		// 只处理启用的配置
		if item.Enable {
			Logger("info", fmt.Sprintf("开始执行文件组【%v】作业...", item.Name))
			// 拼接落地路径
			destDir := conf.Path + item.TargetPath
			// 检查是否落地路径为全局路径
			if item.IsTargetFull {
				destDir = item.TargetPath
			}
			// 替换路径日期
			destDir, err = replaceDateStr(destDir, date)
			if destDir == "" && err != nil && len(err.Error()) != 0 {
				Logger("err", fmt.Sprintf("替换落地路径失败,落地路径%v,输入日期%v", destDir, date))
				continue
			}
			// 创建路径
			err = ensureDir(destDir)
			if err != nil && len(err.Error()) != 0 && checkFileAndDir(destDir) != nil {
				Logger("err", fmt.Sprintf("创建路径%v,失败", destDir), err.Error())
			} else {
				Logger("info", fmt.Sprintf("路径%v创建成功或已存在", destDir))
			}
			// 文件路径类型判断
			switch strings.ToLower(item.ServerType) {
			case "windows":
				// 替换源文件路径日期
				sourceDir, err := replaceDateStr(item.SourcePath, date)
				if sourceDir == "" && err != nil && len(err.Error()) != 0 {
					Logger("err", fmt.Sprintf("替换落地路径失败,落地路径%v,输入日期%v", sourceDir, date))
					continue
				}
				// 获取源文件列表
				var sourceFileList map[string]string
				// 仅有单文件过滤，无排除文件（暂时先这么写，后面重构统一整合）
				if item.SingleFile != nil && item.NotIncluded == nil {
					Logger("info", "准备进行单文件拷贝...")
					// 提前处理单文件通配符
					newSingleFileList, err := replaceFileLitDate(item.SingleFile, date)
					if err != nil && newSingleFileList != nil {
						continue
					}
					// 从配置文件获取单文件路径
					//sourceFileList, err = mergeSingleFilesToList(sourceDir, newSingleFileList)
					sourceFileList, err = getWildCaredFileList(sourceDir, newSingleFileList, true)
					if err != nil && len(err.Error()) != 0 {
						Logger("err", "单文件原路径获取失败，请检查文件配置")
						continue
					}

				} else {
					// 从配置路径获取目录文件
					sourceFileList, err = getFileList(sourceDir)
					if err != nil && len(err.Error()) != 0 {
						Logger("err", fmt.Sprintf("源目录%v访问失败，跳过文件组【%v】", sourceDir, item.Name), err.Error())
						continue
					}

				}
				// 统计源路径总文件数
				cntSourceFiles := len(sourceFileList)

				if conf.Debug {
					debugOutput("原路径文件：", sourceFileList)
				}
				// 检查并过滤不拷贝的文件
				if item.NotIncluded != nil {
					sourceFileList = filterFileListFromMap(sourceFileList, item.NotIncluded)
				}

				// 文件拷贝
				for file, _ := range sourceFileList {
					if err := copyFile(sourceDir+file, destDir+file); err != nil && len(err.Error()) != 0 {
						Logger("err", fmt.Sprintf("源文件：%v拷贝失败，跳过", file), err.Error())
						continue
					}
				}
				// 修改文件日期
				if err := modifyDirFilesTimes(sourceDir, destDir); err != nil {
					continue
				}
				// 文件拷贝校验
				if conf.MD5Check {
					Logger("info", "准备进行文件MD5校验...")
					destFileList := make(map[string]string)
					// 单文件校验例外处理
					if item.SingleFile != nil {
						//destFileList, err = mergeSingleFilesToList(destDir, item.SingleFile)
						destFileList, err = getWildCaredFileList(destDir, item.SingleFile, true)

					} else {
						files, err := getFolderFiles(sourceDir)
						if err != nil {
							continue
						}
						// 过滤文件
						if item.NotIncluded != nil {
							files = filterFileListFromStrArray(files, item.NotIncluded)
						}

						destFileList, err = mergeFilesToList(destDir, files)
					}

					if err != nil && len(err.Error()) != 0 {
						Logger("err", "单文件落地路径获取失败，请检查文件配置", err.Error())
						continue
					}

					if conf.Debug {
						debugOutput("落地路径文件：", destFileList)
					}
					// 完全一致
					if reflect.DeepEqual(sourceFileList, destFileList) {
						Logger("info", fmt.Sprintf("成功拷贝文件组：【%v】，源文件共%v个，本次拷贝共%v个，MD5校验一致", item.Name, cntSourceFiles, len(destFileList)))
						if item.Count != 0 && item.Count != len(destFileList) {
							Logger("warn", fmt.Sprintf("根据配置落地文件应有%v，实际%v，数量不一致，请检查文件数量", item.Count, len(destFileList)))
						}
						// 拷贝无误后进行unzip校验
						if item.Unzip {
							err = unzipFiles(destDir, destDir)
							if err != nil {
								continue
							}
							Logger("info", "文件组解压完成")
							if item.IsDelZip {
								err = delFiles(destDir, "*.zip")
								if err != nil {
									continue
								}
							}
							Logger("info", "Zip文件删除完毕")
						}

					} else {
						// 不一致则输出到日志
						Logger("err", fmt.Sprintf("文件组：【%v】MD5校验异常：", item.Name))
						compareFileList(sourceFileList, destFileList)
						continue
					}
				}
				// 执行cmd命令
				if item.CommandLine != "" {
					cmd, err := replaceDateStr(item.CommandLine, date)
					if err != nil {
						Logger("err", "命令行日期转换失败", err.Error())
					}

					Logger("info", fmt.Sprintf("开始执行命令行：%v", cmd))
					if err = execCommandLine(cmd); err != nil {
						Logger("err", "命令执行失败", err.Error())
					}
				}
			default:
				Logger("err", fmt.Sprintf("配置组%v存在无效的filetype", item.Name))
			}
		}
	}
}
