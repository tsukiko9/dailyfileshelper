package lib

import (
	"DailyFilesHelper/module"
	"archive/zip"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"gopkg.in/yaml.v2"
	"hash/crc32"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"
)

// yaml配置文件读取
func yamlReader(filePtah string) (module.Conf, error) {
	conf := module.Conf{}
	// 读取配置文件
	yamlFile, err := ioutil.ReadFile(filePtah)
	if err != nil {
		return conf, err
	}
	// 解析文件
	if err = yaml.Unmarshal([]byte(yamlFile), &conf); err != nil {
		return conf, err
	}
	return conf, err
}

// 目录校验，没有就创建
func ensureDir(path string) error {
	err := os.MkdirAll(path, os.ModeDir)
	if err != nil {
		return err
	}
	return nil
}

// 获取目录下文件列表
func getFolderFiles(dirPath string) ([]string, error) {

	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}
	var filesList []string
	for _, f := range files {
		// 过滤目录
		if !f.IsDir() {
			filesList = append(filesList, f.Name())
		}
	}
	return filesList, err
}

// 获取目录下文件列表
func getFileList(dirPath string) (map[string]string, error) {

	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}
	filesList := make(map[string]string)
	for _, f := range files {
		// 过滤目录
		if !f.IsDir() {
			tempPath := dirPath + "\\" + f.Name()
			md5str, err := getFileMD5(tempPath)
			if err == nil {
				filesList[f.Name()] = md5str
			} else {
				continue
			}
		}
	}
	return filesList, err
}

// 获取文件MD5
func getFileMD5(filePath string) (string, error) {

	file, err := os.Open(filePath)
	if err != nil {
		Logger("err", fmt.Sprintf("文件%v访问失败", filePath), err.Error())
	}
	defer file.Close()

	md5Hash := md5.New()
	if _, err := io.Copy(md5Hash, file); err != nil {
		Logger("err", fmt.Sprintf("文件%vMD5校验失败", filePath), err.Error())
	}
	md5Str := hex.EncodeToString(md5Hash.Sum(nil))
	return md5Str, err
}

// 获取文件CRC32(暂不启用)
func getFileCRC32(filePath string) (string, error) {
	var returnCRC32String string
	file, err := os.Open(filePath)
	if err != nil {
		Logger("err", fmt.Sprintf("文件%v访问失败", filePath), err.Error())
	}
	defer file.Close()
	tablePolynomial := crc32.MakeTable(0xedb88320)
	hash := crc32.New(tablePolynomial)
	if _, err := io.Copy(hash, file); err != nil {
		Logger("err", fmt.Sprintf("文件%vCRC32校验失败", filePath), err.Error())
	}
	hashInBytes := hash.Sum(nil)[:]
	returnCRC32String = hex.EncodeToString(hashInBytes)
	return returnCRC32String, err
}

// 获取路径下所有文件时间
func getDirFilesTimes(dirPath string) (attr []module.FileAttr, err error) {

	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return
	}
	var list []module.FileAttr
	for _, f := range files {
		stat := f.Sys().(*syscall.Win32FileAttributeData)
		times := module.FileTimes{
			ATime: time.Unix(0, stat.LastAccessTime.Nanoseconds()),
			MTime: time.Unix(0, stat.LastWriteTime.Nanoseconds()),
			BTime: time.Unix(0, stat.CreationTime.Nanoseconds())}
		attr := module.FileAttr{Name: f.Name(), Times: times}
		list = append(list, attr)
	}
	return list, nil
}

// 单文件获取文件时间
func getSingleFileTimes(filePath string) (fileAttr module.FileTimes, err error) {

	file, err := os.Stat(filePath)
	if err != nil {
		return fileAttr, err
	}
	stat := file.Sys().(*syscall.Win32FileAttributeData)
	fileAttr = module.FileTimes{
		ATime: time.Unix(0, stat.LastAccessTime.Nanoseconds()),
		MTime: time.Unix(0, stat.LastWriteTime.Nanoseconds()),
		BTime: time.Unix(0, stat.CreationTime.Nanoseconds())}
	return fileAttr, nil
}

// select in
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// 修改文件时间
// （使用同一组文件名会导致文件修改会若落地文件数量经过滤不一致时，会出现报错，重构时修改）
func modifyDirFilesTimes(sourceDir, destDir string) error {

	// 获取源文件时间
	fileList, err := getDirFilesTimes(sourceDir)
	if err != nil {
		Logger("err", "获取文件列表失败", err.Error())
		return err
	}

	// 获取落地文件列表
	destFiles, err := getFolderFiles(destDir)
	if err != nil {
		Logger("err", "【修改时间】获取落地文件列表失败", err.Error())
	}

	for _, f := range fileList {
		if !stringInSlice(f.Name, destFiles) {
			continue
		}
		err = os.Chtimes(destDir+f.Name, f.Times.ATime, f.Times.MTime)
		if err != nil {
			Logger("err", fmt.Sprintf("文件：%v，修改失败，跳过", destDir+f.Name), err.Error())
			continue
		}
	}
	return nil
}

// 合并多个单文件
func mergeSingleFilesToList(dirPath string, fileList []string) (map[string]string, error) {

	// 先获取路径文件列表
	sourceFileList, err := getFolderFiles(dirPath)
	if err != nil {
		return nil, err
	}

	if len(sourceFileList) > 1000 {
		Logger("warn", fmt.Sprintf("源路径:%v文件数已达%v个，建议清理", dirPath, len(sourceFileList)))
	}
	// 文件组定义
	var wildCardFileList []string // 通配符文件组
	var mergeFileList []string    // 筛选后汇总文件组
	// 通配符列表
	for i := range fileList {
		if strings.Contains(fileList[i], "*") {
			wildCardFileList = append(wildCardFileList, strings.Replace(fileList[i], "*", "", -1))
		}
	}
	if len(wildCardFileList) != 0 {
		Logger("info", fmt.Sprintf("已转换单文件通配符%+v", wildCardFileList))
	}
	// 筛选源路径通配符文件组（迫于为了保证文件名大小写不变，取消使用Map key）
	for i := range wildCardFileList {
		for k := range sourceFileList {
			if strings.Contains(sourceFileList[k], wildCardFileList[i]) {
				mergeFileList = append(mergeFileList, sourceFileList[k])
			}
		}
	}
	// 删除所有通配符，以*为准
	var tempList []string
	for i := range fileList {
		if !strings.Contains(fileList[i], "*") {
			tempList = append(tempList, fileList[i])
		}
	}
	// 重置单文件列表
	fileList = tempList
	// 筛选正常文件组（迫于为了保证文件名大小写不变，取消使用Map key）
	for i := range fileList {
		for k := range sourceFileList {
			if strings.ToLower(fileList[i]) == strings.ToLower(sourceFileList[k]) {
				mergeFileList = append(mergeFileList, sourceFileList[k])

			}
		}

	}

	// 合并待移动文件组
	fileMap := make(map[string]string)

	for i := range mergeFileList {
		MD5Str, err := getFileMD5(dirPath + mergeFileList[i])
		if err != nil {
			return nil, err
		}
		fileMap[mergeFileList[i]] = MD5Str
	}
	return fileMap, nil
}

// 合并目录文件
func mergeFilesToList(dirPath string, fileList []string) (map[string]string, error) {

	// 合并待移动文件组
	fileMap := make(map[string]string)

	for k, _ := range fileList {
		MD5Str, err := getFileMD5(dirPath + fileList[k])
		if err != nil {
			continue
		}
		fileMap[fileList[k]] = MD5Str
	}
	return fileMap, nil
}

// debug 输出
func debugOutput(args ...interface{}) {

	for _, arg := range args {
		switch arg.(type) {
		// 配置文件类型输出
		case module.Conf:
			Logger("debug", fmt.Sprintf("已读取的配置文件信息为：%+v", arg))
		case map[string]string:
			Logger("debug", fmt.Sprintf("%+v", arg))
		case string:
			Logger("debug", fmt.Sprintf("%v", arg))
		default:
			Logger("err", "debug输出异常,未匹配到有效的参数类型...")
		}
	}
}

// 文件复制
func copyFile(sourcePath, targetPath string) error {

	srcFile, err := os.Open(sourcePath)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	destFile, err := os.Create(targetPath)
	if err != nil {
		return err
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, srcFile)
	if err != nil {
		return err
	}

	return err
}

// 获取合并路径
func getFileFullPath(filesPath, dirPath string) ([]string, error) {
	files, err := ioutil.ReadDir(filesPath)
	var fullPathList []string
	if err != nil {
		return nil, err
	}
	for _, item := range files {
		fullPathList = append(fullPathList, dirPath+item.Name())
	}
	return fullPathList, err
}

// 处理日期（粗暴式转换）
func replaceDateStr(dateStr, date string) (string, error) {

	// 日期格式化
	dateStr = strings.ToLower(dateStr)
	formatDate, err := time.Parse("20060102", date)
	if err != nil && len(err.Error()) != 0 {
		Logger("err", fmt.Sprintf("日期%v有误，不是有效的yyyymmdd格式", date), err.Error())
		return "", err
	}
	// 格式化日期参数
	yyyy := formatDate.Format("2006")
	m := formatDate.Format("1")
	mm := formatDate.Format("01")

	d := formatDate.Format("2")
	dd := formatDate.Format("02")

	// 格式化十六进制参数
	hexLowerMoth, _ := strconv.Atoi(m)
	//hexLowerDay, _ := strconv.Atoi(d)

	if strings.Contains(dateStr, "yyyymmdd") {
		// yyyymmdd
		return strings.Replace(dateStr, "yyyymmdd", date, -1), nil
	} else if strings.Contains(dateStr, "yyyy-m-d") {
		// yyyy-m-d
		return strings.Replace(dateStr, "yyyy-m-d", fmt.Sprintf("%v-%v-%v", yyyy, m, d), -1), nil
	} else if strings.Contains(dateStr, "yyyy-mm-dd") {
		// yyyy-mm-dd
		return strings.Replace(dateStr, "yyyy-mm-dd", fmt.Sprintf("%v-%v-%v", yyyy, mm, dd), -1), nil
	} else if strings.Contains(dateStr, "mdd") {
		// mdd 主要用于文件格式替换目前仅支持十六进制
		return strings.Replace(dateStr, "mdd", fmt.Sprintf("%0x%v", hexLowerMoth, dd), -1), nil
	}
	// 否则返回原路径

	return dateStr, err
}

// 处理单文件数组通配符日期
func replaceFileLitDate(fileList []string, date string) ([]string, error) {

	for i := range fileList {

		tempStr, err := replaceDateStr(fileList[i], date)
		if err != nil && len(err.Error()) != 0 {
			Logger("err", fmt.Sprintf("单文件%v, 日期通配符替换失败", fileList[i]), err.Error())
			return nil, err
		}
		fileList[i] = tempStr
	}
	return fileList, nil
}

// unzip thanks for https://stackoverflow.com/questions/20357223/easy-way-to-unzip-file-with-golang
func unzip(srcFile, destPath string) error {

	r, err := zip.OpenReader(srcFile)
	if err != nil {
		return err
	}
	defer func() {
		if err := r.Close(); err != nil {
			panic(err)
		}
	}()

	os.MkdirAll(destPath, 0755)

	// Closure to address file descriptors issue with all the deferred .Close() methods
	extractAndWriteFile := func(f *zip.File) error {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		defer func() {
			if err := rc.Close(); err != nil {
				panic(err)
			}
		}()

		path := filepath.Join(destPath, f.Name)

		if f.FileInfo().IsDir() {
			os.MkdirAll(path, f.Mode())
		} else {
			os.MkdirAll(filepath.Dir(path), f.Mode())
			f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			defer func() {
				if err := f.Close(); err != nil {
					panic(err)
				}
			}()

			_, err = io.Copy(f, rc)
			if err != nil {
				return err
			}
		}
		return nil
	}

	for _, f := range r.File {
		err := extractAndWriteFile(f)
		if err != nil {
			return err
		}
	}

	return nil
}

// unzip路径下所有zip文件
func unzipFiles(srcDir, destDir string) error {
	// 获取路径文件
	fileList, err := getFolderFiles(srcDir)
	if err != nil {
		return err
	}
	// 剔除非zip文件
	for i := range fileList {
		if strings.Contains(strings.ToLower(fileList[i]), "zip") {
			err = unzip(srcDir+fileList[i], destDir)
			if err != nil && len(err.Error()) != 0 {
				Logger("err", fmt.Sprintf("文件：%v解压失败，跳过"), srcDir+fileList[i])
				return err
			}
		}
	}
	return nil
}

// 删除指定通配符文件
func delFiles(dir, wildCardStr string) error {
	files, err := filepath.Glob(dir + wildCardStr)
	if err != nil {
		Logger("err", fmt.Sprintf("删除通配符%v访问失败", dir+wildCardStr), err.Error())
		return err
	}
	for _, f := range files {
		if err := os.Remove(f); err != nil {
			Logger("err", fmt.Sprintf("删除路径%v访问失败", f), err.Error())
			return err
		}
	}
	return nil
}

// 文件拷贝通配符文件筛选
func getWildCaredFileList(sourceDir string, matchList []string, isIncluded bool) (map[string]string, error) {

	// 先获取路径文件列表
	sourceFileList, err := getFolderFiles(sourceDir)
	if err != nil {
		return nil, err
	}

	if len(sourceFileList) > 1000 {
		Logger("warn", fmt.Sprintf("源路径:%v文件数已达%v个，建议清理", sourceDir, len(sourceFileList)))
	}
	var wildCardFileList []string // 通配符文件组
	var mergeFileList []string    // 筛选后汇总文件组
	isIncluded = true

	// 获取通配符文件列表
	for i := range matchList {
		if strings.Contains(matchList[i], "*") {
			wildCardFileList = append(wildCardFileList, strings.Replace(matchList[i], "*", "", -1))
		}
	}
	if len(wildCardFileList) != 0 {
		Logger("info", fmt.Sprintf("通配符所匹配文件列表%+v", wildCardFileList))
	}
	// 找到通配符匹配的文件（低效率嵌套循环，不能用key莫得法子）
	for i := range wildCardFileList {
		for k := range sourceFileList {
			// 匹配通配符文件
			if strings.Contains(sourceFileList[k], wildCardFileList[i]) {
				if isIncluded {
					// 包含
					mergeFileList = append(mergeFileList, sourceFileList[k])
				} else {
					// 不包含
					continue
				}
			}
		}
	}

	// 删除所有通配符，以*为准
	var tempList []string
	for i := range matchList {
		if !strings.Contains(matchList[i], "*") {
			tempList = append(tempList, matchList[i])
		}
	}
	// 重置单文件列表
	matchList = tempList
	// 筛选正常文件组（迫于为了保证文件名大小写不变，取消使用Map key）
	for i := range matchList {
		for k := range sourceFileList {
			if strings.ToLower(matchList[i]) == strings.ToLower(sourceFileList[k]) && isIncluded {
				mergeFileList = append(mergeFileList, sourceFileList[k])

			} else if !isIncluded {
				continue
			}
		}

	}

	// 合并待移动文件组
	fileMap := make(map[string]string)

	for i := range mergeFileList {
		MD5Str, err := getFileMD5(sourceDir + mergeFileList[i])
		if err != nil {
			return nil, err
		}
		fileMap[mergeFileList[i]] = MD5Str
	}

	return fileMap, nil
}

// 过滤Map文件列表中特定的文件
func filterFileListFromMap(sourceFileMap map[string]string, filterFiles []string) map[string]string {

	var filter []string
	//根据匹配关系取出需要删除的文件名称
	for i := range filterFiles {
		for k, _ := range sourceFileMap {
			if strings.Contains(k, filterFiles[i]) {
				filter = append(filter, k)
			}
		}
	}
	Logger("info", fmt.Sprintf("过滤源文件：%+v", filter))
	for i := range filter {
		delete(sourceFileMap, filter[i])
	}

	return sourceFileMap
}

func removeIndex(slice []string, s int) []string {
	return append(slice[:s], slice[s+1:]...)
}

// 过滤string array文件列表中特定的文件
func filterFileListFromStrArray(sourceArray []string, filterFiles []string) []string {

	var filter []string
	// 获取需要过滤的文件列表
	for i := range filterFiles {
		for j := range sourceArray {
			if strings.Contains(sourceArray[j], filterFiles[i]) {
				filter = append(filter, sourceArray[j])
			}

		}
	}

	// 过滤后文件列表
	var tempArray []string
	for i := range sourceArray {
		if stringInSlice(sourceArray[i], filter) {
			continue
		}
		tempArray = append(tempArray, sourceArray[i])
	}
	sourceArray = tempArray
	return sourceArray
}

// 命令行可执行
func execCommandLine(commandLine string) error {

	cmd := exec.Command(commandLine)
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}
