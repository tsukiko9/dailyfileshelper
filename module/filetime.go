package module

import "time"

type FileAttr struct {
	Name  string
	Times FileTimes
}

type FileTimes struct {
	ATime time.Time
	MTime time.Time
	BTime time.Time
}
