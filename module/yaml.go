package module

type FilesList struct {
	Name         string   `yaml:"name"`         // 组名
	Enable       bool     `yaml:"enable"`       // 是否启用
	ServerType   string   `yaml:"servertype"`   // 文件获取类型
	SourcePath   string   `yaml:"sourcepath"`   // 源文件路径
	TargetPath   string   `yaml:"targetpath"`   // 文件落地路径
	IsTargetFull bool     `yaml:"istargetfull"` // 是否启用绝对路径，不启用则为以path为根目录的二级目录
	SingleFile   []string `yaml:"singlefile"`   // 是否只拷贝单个文件
	Unzip        bool     `yaml:"unzip"`        // 是否需要unZip
	IsDelZip     bool     `yaml:"isdelzip"`     // 是否解压后删除zip文件
	Count        int      `yaml:"count"`        // 统计应有的拷贝文件数
	NotIncluded  []string `yaml:"notincluded"`  // 不包含的文件名列表
	CommandLine  string   `yaml:"commandline"`  // 可执行命令行
}
type Conf struct {
	NameList  []string    `yaml:"namelist"`  // 名称列表，与组名对应
	Path      string      `yaml:"path"`      // 文件落地根目录
	MD5Check  bool        `yaml:"md5check"`  // 是否启用hash校验
	Debug     bool        `yaml:"debug"`     // 开发者模式
	FilesList []FilesList `yaml:"fileslist"` // 配置组列表
}
